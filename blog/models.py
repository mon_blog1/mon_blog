from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

# Create your models here.

class Entree(models.Model):
    titre = models.CharField(max_length=22,unique=True)
    text = models.TextField()
    date = models.DateField(auto_now_add=True)
    auteur = models.ForeignKey(User, on_delete= models.CASCADE)

    class Meta:
        verbose_name_plural = "entrées"

    def __str__(self):
        return (self.titre)
